import React from "react";
export default function Header() {
    return (
      <div className="header" style={{boxSizing: "border-box",
        width: "100%",
        display: "flex",
        justifyContent: "center",
        padding: "1em",
        marginBottom: "2em",
        backgroundColor: "blue",
        color: "#fff"}}>
       
          <h1>Pictora App</h1>
        
      </div>
    )
  }