import React from 'react';
import PropTypes from 'prop-types';
import Demo from '../components/Demo.js';

import {notify} from 'react-notify-toast';
import './styles/imagePreview.css';

function dataURItoBlob(dataURI) {
  // convert base64/URLEncoded data component to raw binary data held in a string
  var byteString;
  if (dataURI.split(',')[0].indexOf('base64') >= 0)
      byteString = atob(dataURI.split(',')[1]);
  else
      byteString = unescape(dataURI.split(',')[1]);

  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

  // write the bytes of the string to a typed array
  var ia = new Uint8Array(byteString.length);
  for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
  }

  return new Blob([ia], {type:mimeString});
}

export const ImagePreview = ({ dataUri, isFullscreen }) => {
  let classNameFullscreen = isFullscreen ? 'demo-image-preview-fullscreen' : '';


 const upload = () => {
   const file = new File ([dataURItoBlob(dataUri)],"mypic.jpg",{
     type: "image/jpeg"
   });
   const fd = new FormData();
   fd.append("mypic",file);
   fetch("http://localhost:8080/uploadProfilePicture",
   {
     method: 'POST',
     body: fd
   }
   ).then(response => {
      
      response.text().then(t=> notify.show(t,"success",2000))
   }).catch(error => {
     notify.show(error.message,"error",2000);
     localStorage.setItem("pic"+"-"+Date.now(),dataUri)
   });
 }
  return (
    <div className={'demo-image-preview ' + classNameFullscreen}>
      <img src={dataUri} />
    <div className="container">
      <button type="submit" onClick={upload} style={{
          justifyContent: "center",
          alignItems: "center",borderRadius: "8px",height:"30px"}}  >Submit</button>
     
      <Demo />
   </div>
    
    </div>
  );
};

ImagePreview.propTypes = {
  dataUri: PropTypes.string,
  isFullscreen: PropTypes.bool,
};

export default ImagePreview;