import React, { Component } from "react";

import CameraComponent from './components/CameraComponent.js';
import SplashScreen from './components/SplashScreen.js';
import HeaderComponent from './components/HeaderComponent.js';
import Notifications  from 'react-notify-toast';

import "./App.css";



class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayCamera: false,
      isButtonDisabled: false,
      renderSplashscreen: true
    };
    this.onButtonClick = this.onButtonClick.bind(this);
    }
    
    onButtonClick() {
      this.setState({
    
        displayCamera: true,
        isButtonDisabled: true
      });
    }
    async componentDidMount() {
      
       
        setTimeout(() => {
          this.setState({
            renderSplashscreen: false,
          });
        }, 2500)
      
    }
    
  render() {
     if(this.state.renderSplashscreen)
      return <SplashScreen/>;
      
    return (
      <div className="App" >
        <Notifications />
       <HeaderComponent />
         <button style={{
          justifyContent: "center",
          alignItems: "center",borderRadius: "8px",height:"30px"}} 
          onClick={this.onButtonClick} 
          disabled={this.state.isButtonDisabled}>Camera</button>
         {this.state.displayCamera ?
           <CameraComponent  /> :
           null
        }
      </div>
    );
  }
  
}

export default App;
